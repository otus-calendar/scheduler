module scheduler

go 1.13

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/jpillora/backoff v0.0.0-20180909062703-3050d21c67d7
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/shopspring/decimal v0.0.0-20190905144223-a36b5d85f337 // indirect
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	golang.org/x/crypto v0.0.0-20190923035154-9ee001bba392 // indirect
	google.golang.org/appengine v1.6.3 // indirect
)
