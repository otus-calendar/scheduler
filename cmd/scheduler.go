package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/jpillora/backoff"
	"github.com/kelseyhightower/envconfig"

	"scheduler/core"
	"scheduler/internal"
)

type Config struct {
	Queue    string        `default:"amqp://guest:guest@localhost:5672/"`
	Database string        `default:"postgres://postgres@localhost/calendar?sslmode=disable"`
	Period   time.Duration `default:"5s"`
}

func main() {
	config := &Config{}
	_ = envconfig.Process("scheduler", config)

	// use signal handler and context with cancel for graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	go handleSignals(signals, cancel)

	backoff := backoff.Backoff{}
	log.Print("service started")
	defer log.Print("service stopped")
	for {
		select {
		case <-ctx.Done():
			return
		default:
			queue, err := internal.NewAMQPQueue(config.Queue)
			if err != nil {
				d := backoff.Duration()
				log.Printf("unable to create queue: %s, retry after %v\n", err, d)
				time.Sleep(d)
				continue
			}
			log.Println("successfully connected to queue")
			repo, err := internal.NewPostgresRepository(ctx, config.Database)
			if err != nil {
				d := backoff.Duration()
				log.Printf("unable to create repository: %s, retry after %v\n", err, d)
				time.Sleep(d)
				continue
			}
			log.Println("successfully connected to database")
			backoff.Reset()
			scheduler := core.Sheduler{
				Period:     config.Period,
				Queue:      queue,
				Repository: repo,
			}
			err = scheduler.Serve(ctx)
			if err != nil {
				log.Printf("server stopped with error: %s\n", err)
			}
		}
	}
}

// handleSignal - cancel context on signal
func handleSignals(signals chan os.Signal, cancel context.CancelFunc) {
	<-signals
	cancel()
	log.Println("stopping service...")
}
