package internal

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"github.com/streadway/amqp"

	"scheduler/core"
)

// rabbit MQ implementation
type AMQPQueue struct {
	conn *amqp.Connection
	ch   *amqp.Channel
	q    amqp.Queue
}

func (q *AMQPQueue) declareQueue() (amqp.Queue, error) {
	return q.ch.QueueDeclare(
		"notifications", // name
		true,            // durable
		false,           // delete when usused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
}

// Close resources
func (q *AMQPQueue) Close() error {
	var chErr, connErr error
	if q.ch != nil {
		chErr = q.ch.Close()
	}
	if q.conn != nil {
		connErr = q.conn.Close()
	}
	if chErr != nil || connErr != nil {
		return errors.New("failed to close queue")
	}
	return nil
}

func (q *AMQPQueue) EnqueueNotification(notification core.Notification) error {
	body, err := json.Marshal(notification)
	if err != nil {
		return err
	}
	err = q.ch.Publish(
		"",       // exchange
		q.q.Name, // routing key
		false,    // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         body,
		})
	if err != nil {
		log.Printf("Failed to publish a message: %s", err)
		return err
	}
	log.Printf("%v enqueued", notification)
	return nil
}

// NewAMQPQueue - rabbit queue constructor
func NewAMQPQueue(connString string) (*AMQPQueue, error) {
	queue := &AMQPQueue{}
	// open connection
	conn, err := amqp.Dial(connString)
	if err != nil {
		return queue, fmt.Errorf("unable to connect to the queue: %w", err)
	}
	queue.conn = conn

	// create channel
	ch, err := conn.Channel()
	if err != nil {
		return queue, fmt.Errorf("unable to create queue channel: %w", err)
	}
	queue.ch = ch

	// declare queue
	q, err := queue.declareQueue()
	if err != nil {
		log.Fatalln("unable to declare queue: ", err)
	}
	queue.q = q
	return queue, nil
}
