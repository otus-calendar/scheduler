.PHONY: build

build:
	go build -o build/scheduler cmd/*

lint:
	golangci-lint run ./...
