package core

import (
	"context"
	"log"
	"time"
)

type Sheduler struct {
	Queue      Queue
	Repository Repository
	Period     time.Duration
}

func (s *Sheduler) Serve(ctx context.Context) error {
	defer s.Queue.Close()
	defer s.Repository.Close()

	ticker := time.NewTicker(s.Period)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return nil
		case <-ticker.C:
			notifications, err := s.Repository.GetNotifications(ctx)
			if err != nil {
				return err
			}
			for _, notification := range notifications {
				err := s.Queue.EnqueueNotification(notification)
				if err != nil {
					log.Printf("error during enqueueing %s\n", err)
				}
			}
		}
	}
}
