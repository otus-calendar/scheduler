package core

type Notification struct {
	Address string
	Content string
}
